//
//  Subframework.h
//  Subframework
//
//  Created by ganesh faterpekar on 1/22/20.
//  Copyright © 2020 Streethawk. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Subframework.
FOUNDATION_EXPORT double SubframeworkVersionNumber;

//! Project version string for Subframework.
FOUNDATION_EXPORT const unsigned char SubframeworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Subframework/PublicHeader.h>


